# ForenHosting.net

![](screenshot.png)

**License:** GCC BY-NC-SA 3.0 (https://creativecommons.org/licenses/by-nc-sa/3.0/)

## Background

On ForenHosting.net you could create your own free forum with my [own](https://github.com/ForenSoftware/forensoftware) Forensoftware or with phpBB in the past.